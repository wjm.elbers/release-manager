package commands

import (
	"github.com/spf13/cobra"
	"release-manager/server"
)

func InitStartCommand(verbose *bool) (*cobra.Command) {
	var port int
	var hostname string

	var cmd = &cobra.Command{
		Use:   "start",
		Short: "start server",
		Long: `Start server`,
		Run: func(cmd *cobra.Command, args []string) {
			server.StartServerAndblock(port, hostname)
		},
	}

	cmd.Flags().IntVarP(&port, "port", "p", 9000, "Base port to run the server on")
	cmd.Flags().StringVarP(&hostname, "hostname", "H", "", "Hostname to deploy application on")

	return cmd;
}

package commands

import (
	"fmt"
	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the release manager version number",
	Long:  `All software has versions. This is release manager's.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		printCheckerVersion()
		return nil
	},
}

func printCheckerVersion() {
	fmt.Printf("Release manager server v%s by CLARIN ERIC\n", "1.0.0-beta")
}

package server

import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"fmt"
	"io/ioutil"
	"encoding/json"
)

func StartServerAndblock(port int, hostname string) {
	router := &mux.Router{}
	router.HandleFunc("/webhooks", handleWebhooks).Methods("GET", "POST", "PUT", "DELETE", "HEAD")

	//Configure CORS
	originsOk := []string{"*"}
	c := cors.New(cors.Options{
		AllowedOrigins: originsOk,
		AllowCredentials: true,
	})

	//Start server
	address := fmt.Sprintf("%s:%d", hostname, port)
	log.Printf("Starting http server on: %s\n", address)
	log.Fatal(http.ListenAndServe(address, c.Handler(router)))
}

func handleWebhooks(w http.ResponseWriter, r *http.Request) {
	requestLogger(r)
}

func requestLogger(r *http.Request) {
	method := r.Method
	uri := r.RequestURI
	referer := r.Referer()

	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		fmt.Printf("Failed to read request body. Error: %s\n", err)
		return
	}

	fmt.Printf("Received webhook\n")
	fmt.Printf("  Method=%s, URI=%s, referer=%s\n", method, uri, referer)

	//
	github_body := GithubReleaseBody{}
	err = json.Unmarshal(b, &github_body)
	if err != nil {
		fmt.Printf("  Failed to unmarshall request body. Error: %s\n", err)
		fmt.Printf("  Body:\n%s\n", string(b))
		return
	}

	//Print result
	result, err := json.MarshalIndent(github_body, "", "\t")
	if err != nil {
		fmt.Printf("  Failed to marshall struct to json. Error: %s\n", err)
		return
	}
	fmt.Printf("  Body:\n%s\n", string(result))
}

type GithubReleaseBody struct {
	Action string `json:"action"`
	Release *GithubRelease `json:"release"`
	Repository *GithubRepository `json:"repository"`
}

type GithubRelease struct {
	Id int64 `json:"id"`
	TagName string `json:"tag_name"`
}

type GithubRepository struct {
	Id int64 `json:"id"`
	Name string `json:"name"`
	FullName string `json:"full_name"`
}

/**
{
	"zen": "Approachable is better than simple.",
	"hook_id": 81171914,
	"hook": {
		"type": "Repository",
		"id": 81171914,
		"name": "web",
		"active": true,
		"events": ["release"],
		"config": {
			"content_type": "json",
			"insecure_ssl": "0",
			"url": "http://130.183.206.39/webhooks"
		},
		"updated_at": "2019-01-24T18:50:24Z",
		"created_at": "2019-01-24T18:50:24Z",
		"url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/hooks/81171914",
		"test_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/hooks/81171914/test",
		"ping_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/hooks/81171914/pings",
		"last_response": {
			"code": null,
			"status": "unused",
			"message": null
		}
	},
	"repository": {
		"id": 50098768,
		"node_id": "MDEwOlJlcG9zaXRvcnk1MDA5ODc2OA==",
		"name": "VirtualCollectionRegistry",
		"full_name": "clarin-eric/VirtualCollectionRegistry",
		"private": false,
		"owner": {
			"login": "clarin-eric",
			"id": 8073499,
			"node_id": "MDEyOk9yZ2FuaXphdGlvbjgwNzM0OTk=",
			"avatar_url": "https://avatars2.githubusercontent.com/u/8073499?v=4",
			"gravatar_id": "",
			"url": "https://api.github.com/users/clarin-eric",
			"html_url": "https://github.com/clarin-eric",
			"followers_url": "https://api.github.com/users/clarin-eric/followers",
			"following_url": "https://api.github.com/users/clarin-eric/following{/other_user}",
			"gists_url": "https://api.github.com/users/clarin-eric/gists{/gist_id}",
			"starred_url": "https://api.github.com/users/clarin-eric/starred{/owner}{/repo}",
			"subscriptions_url": "https://api.github.com/users/clarin-eric/subscriptions",
			"organizations_url": "https://api.github.com/users/clarin-eric/orgs",
			"repos_url": "https://api.github.com/users/clarin-eric/repos",
			"events_url": "https://api.github.com/users/clarin-eric/events{/privacy}",
			"received_events_url": "https://api.github.com/users/clarin-eric/received_events",
			"type": "Organization",
			"site_admin": false
		},
		"html_url": "https://github.com/clarin-eric/VirtualCollectionRegistry",
		"description": "Virtual Collection Registry (VCR)",
		"fork": false,
		"url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry",
		"forks_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/forks",
		"keys_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/keys{/key_id}",
		"collaborators_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/collaborators{/collaborator}",
		"teams_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/teams",
		"hooks_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/hooks",
		"issue_events_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/issues/events{/number}",
		"events_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/events",
		"assignees_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/assignees{/user}",
		"branches_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/branches{/branch}",
		"tags_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/tags",
		"blobs_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/blobs{/sha}",
		"git_tags_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/tags{/sha}",
		"git_refs_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/refs{/sha}",
		"trees_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/trees{/sha}",
		"statuses_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/statuses/{sha}",
		"languages_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/languages",
		"stargazers_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/stargazers",
		"contributors_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/contributors",
		"subscribers_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/subscribers",
		"subscription_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/subscription",
		"commits_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/commits{/sha}",
		"git_commits_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/commits{/sha}",
		"comments_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/comments{/number}",
		"issue_comment_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/issues/comments{/number}",
		"contents_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/contents/{+path}",
		"compare_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/compare/{base}...{head}",
		"merges_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/merges",
		"archive_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/{archive_format}{/ref}",
		"downloads_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/downloads",
		"issues_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/issues{/number}",
		"pulls_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/pulls{/number}",
		"milestones_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/milestones{/number}",
		"notifications_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/notifications{?since,all,participating}",
		"labels_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/labels{/name}",
		"releases_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/releases{/id}",
		"deployments_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/deployments",
		"created_at": "2016-01-21T10:03:51Z",
		"updated_at": "2016-01-21T12:03:39Z",
		"pushed_at": "2019-01-24T13:57:11Z",
		"git_url": "git://github.com/clarin-eric/VirtualCollectionRegistry.git",
		"ssh_url": "git@github.com:clarin-eric/VirtualCollectionRegistry.git",
		"clone_url": "https://github.com/clarin-eric/VirtualCollectionRegistry.git",
		"svn_url": "https://github.com/clarin-eric/VirtualCollectionRegistry",
		"homepage": null,
		"size": 2378,
		"stargazers_count": 0,
		"watchers_count": 0,
		"language": "Java",
		"has_issues": true,
		"has_projects": true,
		"has_downloads": true,
		"has_wiki": true,
		"has_pages": false,
		"forks_count": 0,
		"mirror_url": null,
		"archived": false,
		"open_issues_count": 12,
		"license": null,
		"forks": 0,
		"open_issues": 12,
		"watchers": 0,
		"default_branch": "master"
	},
	"sender": {
		"login": "WillemElbers",
		"id": 4987451,
		"node_id": "MDQ6VXNlcjQ5ODc0NTE=",
		"avatar_url": "https://avatars2.githubusercontent.com/u/4987451?v=4",
		"gravatar_id": "",
		"url": "https://api.github.com/users/WillemElbers",
		"html_url": "https://github.com/WillemElbers",
		"followers_url": "https://api.github.com/users/WillemElbers/followers",
		"following_url": "https://api.github.com/users/WillemElbers/following{/other_user}",
		"gists_url": "https://api.github.com/users/WillemElbers/gists{/gist_id}",
		"starred_url": "https://api.github.com/users/WillemElbers/starred{/owner}{/repo}",
		"subscriptions_url": "https://api.github.com/users/WillemElbers/subscriptions",
		"organizations_url": "https://api.github.com/users/WillemElbers/orgs",
		"repos_url": "https://api.github.com/users/WillemElbers/repos",
		"events_url": "https://api.github.com/users/WillemElbers/events{/privacy}",
		"received_events_url": "https://api.github.com/users/WillemElbers/received_events",
		"type": "User",
		"site_admin": false
	}
}
 */


 /**
 Received webhook
  Method=POST, URI=/webhooks, referer=
  Body:
{
	"action": "published",
	"release": {
		"url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/releases/15166779",
		"assets_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/releases/15166779/assets",
		"upload_url": "https://uploads.github.com/repos/clarin-eric/VirtualCollectionRegistry/releases/15166779/assets{?name,label}",
		"html_url": "https://github.com/clarin-eric/VirtualCollectionRegistry/releases/tag/Test",
		"id": 15166779,
		"node_id": "MDc6UmVsZWFzZTE1MTY2Nzc5",
		"tag_name": "Test",
		"target_commitish": "milestone-1.2",
		"name": "",
		"draft": false,
		"author": {
			"login": "WillemElbers",
			"id": 4987451,
			"node_id": "MDQ6VXNlcjQ5ODc0NTE=",
			"avatar_url": "https://avatars2.githubusercontent.com/u/4987451?v=4",
			"gravatar_id": "",
			"url": "https://api.github.com/users/WillemElbers",
			"html_url": "https://github.com/WillemElbers",
			"followers_url": "https://api.github.com/users/WillemElbers/followers",
			"following_url": "https://api.github.com/users/WillemElbers/following{/other_user}",
			"gists_url": "https://api.github.com/users/WillemElbers/gists{/gist_id}",
			"starred_url": "https://api.github.com/users/WillemElbers/starred{/owner}{/repo}",
			"subscriptions_url": "https://api.github.com/users/WillemElbers/subscriptions",
			"organizations_url": "https://api.github.com/users/WillemElbers/orgs",
			"repos_url": "https://api.github.com/users/WillemElbers/repos",
			"events_url": "https://api.github.com/users/WillemElbers/events{/privacy}",
			"received_events_url": "https://api.github.com/users/WillemElbers/received_events",
			"type": "User",
			"site_admin": false
		},
		"prerelease": true,
		"created_at": "2019-01-24T13:55:38Z",
		"published_at": "2019-01-24T19:02:40Z",
		"assets": [],
		"tarball_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/tarball/Test",
		"zipball_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/zipball/Test",
		"body": ""
	},
	"repository": {
		"id": 50098768,
		"node_id": "MDEwOlJlcG9zaXRvcnk1MDA5ODc2OA==",
		"name": "VirtualCollectionRegistry",
		"full_name": "clarin-eric/VirtualCollectionRegistry",
		"private": false,
		"owner": {
			"login": "clarin-eric",
			"id": 8073499,
			"node_id": "MDEyOk9yZ2FuaXphdGlvbjgwNzM0OTk=",
			"avatar_url": "https://avatars2.githubusercontent.com/u/8073499?v=4",
			"gravatar_id": "",
			"url": "https://api.github.com/users/clarin-eric",
			"html_url": "https://github.com/clarin-eric",
			"followers_url": "https://api.github.com/users/clarin-eric/followers",
			"following_url": "https://api.github.com/users/clarin-eric/following{/other_user}",
			"gists_url": "https://api.github.com/users/clarin-eric/gists{/gist_id}",
			"starred_url": "https://api.github.com/users/clarin-eric/starred{/owner}{/repo}",
			"subscriptions_url": "https://api.github.com/users/clarin-eric/subscriptions",
			"organizations_url": "https://api.github.com/users/clarin-eric/orgs",
			"repos_url": "https://api.github.com/users/clarin-eric/repos",
			"events_url": "https://api.github.com/users/clarin-eric/events{/privacy}",
			"received_events_url": "https://api.github.com/users/clarin-eric/received_events",
			"type": "Organization",
			"site_admin": false
		},
		"html_url": "https://github.com/clarin-eric/VirtualCollectionRegistry",
		"description": "Virtual Collection Registry (VCR)",
		"fork": false,
		"url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry",
		"forks_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/forks",
		"keys_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/keys{/key_id}",
		"collaborators_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/collaborators{/collaborator}",
		"teams_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/teams",
		"hooks_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/hooks",
		"issue_events_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/issues/events{/number}",
		"events_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/events",
		"assignees_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/assignees{/user}",
		"branches_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/branches{/branch}",
		"tags_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/tags",
		"blobs_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/blobs{/sha}",
		"git_tags_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/tags{/sha}",
		"git_refs_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/refs{/sha}",
		"trees_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/trees{/sha}",
		"statuses_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/statuses/{sha}",
		"languages_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/languages",
		"stargazers_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/stargazers",
		"contributors_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/contributors",
		"subscribers_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/subscribers",
		"subscription_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/subscription",
		"commits_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/commits{/sha}",
		"git_commits_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/git/commits{/sha}",
		"comments_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/comments{/number}",
		"issue_comment_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/issues/comments{/number}",
		"contents_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/contents/{+path}",
		"compare_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/compare/{base}...{head}",
		"merges_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/merges",
		"archive_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/{archive_format}{/ref}",
		"downloads_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/downloads",
		"issues_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/issues{/number}",
		"pulls_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/pulls{/number}",
		"milestones_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/milestones{/number}",
		"notifications_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/notifications{?since,all,participating}",
		"labels_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/labels{/name}",
		"releases_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/releases{/id}",
		"deployments_url": "https://api.github.com/repos/clarin-eric/VirtualCollectionRegistry/deployments",
		"created_at": "2016-01-21T10:03:51Z",
		"updated_at": "2016-01-21T12:03:39Z",
		"pushed_at": "2019-01-24T19:02:40Z",
		"git_url": "git://github.com/clarin-eric/VirtualCollectionRegistry.git",
		"ssh_url": "git@github.com:clarin-eric/VirtualCollectionRegistry.git",
		"clone_url": "https://github.com/clarin-eric/VirtualCollectionRegistry.git",
		"svn_url": "https://github.com/clarin-eric/VirtualCollectionRegistry",
		"homepage": null,
		"size": 2378,
		"stargazers_count": 0,
		"watchers_count": 0,
		"language": "Java",
		"has_issues": true,
		"has_projects": true,
		"has_downloads": true,
		"has_wiki": true,
		"has_pages": false,
		"forks_count": 0,
		"mirror_url": null,
		"archived": false,
		"open_issues_count": 12,
		"license": null,
		"forks": 0,
		"open_issues": 12,
		"watchers": 0,
		"default_branch": "master"
	},
	"organization": {
		"login": "clarin-eric",
		"id": 8073499,
		"node_id": "MDEyOk9yZ2FuaXphdGlvbjgwNzM0OTk=",
		"url": "https://api.github.com/orgs/clarin-eric",
		"repos_url": "https://api.github.com/orgs/clarin-eric/repos",
		"events_url": "https://api.github.com/orgs/clarin-eric/events",
		"hooks_url": "https://api.github.com/orgs/clarin-eric/hooks",
		"issues_url": "https://api.github.com/orgs/clarin-eric/issues",
		"members_url": "https://api.github.com/orgs/clarin-eric/members{/member}",
		"public_members_url": "https://api.github.com/orgs/clarin-eric/public_members{/member}",
		"avatar_url": "https://avatars2.githubusercontent.com/u/8073499?v=4",
		"description": "CLARIN central source code hub"
	},
	"sender": {
		"login": "WillemElbers",
		"id": 4987451,
		"node_id": "MDQ6VXNlcjQ5ODc0NTE=",
		"avatar_url": "https://avatars2.githubusercontent.com/u/4987451?v=4",
		"gravatar_id": "",
		"url": "https://api.github.com/users/WillemElbers",
		"html_url": "https://github.com/WillemElbers",
		"followers_url": "https://api.github.com/users/WillemElbers/followers",
		"following_url": "https://api.github.com/users/WillemElbers/following{/other_user}",
		"gists_url": "https://api.github.com/users/WillemElbers/gists{/gist_id}",
		"starred_url": "https://api.github.com/users/WillemElbers/starred{/owner}{/repo}",
		"subscriptions_url": "https://api.github.com/users/WillemElbers/subscriptions",
		"organizations_url": "https://api.github.com/users/WillemElbers/orgs",
		"repos_url": "https://api.github.com/users/WillemElbers/repos",
		"events_url": "https://api.github.com/users/WillemElbers/events{/privacy}",
		"received_events_url": "https://api.github.com/users/WillemElbers/received_events",
		"type": "User",
		"site_admin": false
	}
}
  */